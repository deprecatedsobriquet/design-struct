#!/usr/bin/env python


class Task(object):
    def __init__(self, label, api, depends=None):
        self.label = label
        self.api = api
        self.dependencies = depends # expect list of task names
        self.nruns = 0 # TODO: inc on successful run, compare against number in task_ord to verify 'completion'?
        self.start_dt = None
        self.end_dt = None
        self.runtime = 0

    def run(self):
        self.start_dt = dt.now() # TODO: logging?
        self.api.run()
        self.end_dt = dt.now()
        self.runtime = self.end_dt-self.start_dt

    def queue(self, jobfile):
        # TODO: method to queue job on cluster?
        pass