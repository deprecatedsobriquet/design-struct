#!/usr/bin/env python

import string
import sys
import os



""" legacy CLI tool, need to clean up

TODO: 


"""

import numpy as _np
from scipy.integrate import ode, odeint
import matplotlib.pyplot as _plt

#from ..shared import *



def get_max_u(y, u):

    idx = _np.argmax(u)

    return idx, y[idx], u[idx]


def get_bl_thk(y, u, U_ref, p=99):

    u_bl = (p/100.)*U_ref

    diff = u_bl - u
    
    idx = _np.argmin( _np.abs(diff) )

    return idx, y[idx]


def get_disp_thk(y, u, U_ref):
    """
    Displacement Thickness

    delta star
    """
    u_n = u/U_ref
    f = (1. - u_n)

    return _np.trapz(f, x=y)


def get_disp_thk_cmp(y, rho, rho_ref, u, U_ref):
    """
    Compressible Displacement Thickness

    delta star
    """
    r_n = rho/rho_ref
    u_n = u/U_ref
    f = (1. - r_n*u_n)

    return _np.trapz(f, x=y)


def get_mo_thick(y, u, U_ref):
    """ 
    Momentum Thickness
    """
    
    u_n = u/U_ref
    d = 1. - u_n
    
    f = u_n*d

    return _np.trapz(f, x=y)

def get_mo_thk_cmp(y, rho, rho_ref, u, U_ref):
    """
    Compressible Momentum Thickness
    """

    r_n = rho/rho_ref
    u_n = u/U_ref
    d = 1. - u_n    
    
    f = r_n*u_n*d

    return _np.trapz(f, x=y)


def get_shape_factor(dstar, theta):
    return dstar/theta


def get_u_star(tau_w, rho_w):
    """
    Friction Velocity
    """
    return _np.sqrt(tau_w/rho_w)


def get_y_plus(y, u_star, nu_w):
    """
    Wall Units for Length Scale y
    """
    return y*u_star/nu_w


def get_van_driest_1(y_plus, kappa=0.41, C=5.0):
    """
    From Huang and Coleman AIAA 1994

    returns u/u_tau
    """
    return (1./kappa)*_np.log(y_plus) + C

def get_van_driest_2(u_plus, B_q, M_tau, gamma=1.4, Pr_t=0.9):
    """
    From Huang and Coleman AIAA 1994

    return u/u_tau
    """
    R = M_tau*_np.sqrt((gamma-1.)*Pr_t/2.)
    H = B_q/((gamma-1.)*M_tau**2.)
    D = _np.sqrt(1.+R**2.*H**2.)

    return (1./R)*( _np.arcsin(R*(u_plus+H)/D) - _np.arcsin(R*H/D) )

def get_van_driest(u_plus, U_e, Ma_e, T_e, T_w, T_aw, gamma=1.4):
    """
    From White Visc. Flows
    """
    a = _np.sqrt((gamma-1.)/2.*Ma_e**2.*T_e/T_w)
    b = (T_aw/T_w - 1.)
    Q = _np.sqrt(B**2 + 4.*a**2)

    return (1./a)( _np.arcsin( (2.*a**2.*u_plus-b)/Q ) + _np.arcsin(b/Q) )

def get_walz_temp(u, u_ref, Ma, T_w, T_aw, r=0.8, gamm=1.4):
    """
    Walz Boundary Layers of Flow and Temperature 1969
    """
    u_n = u/u_ref

    u_n2 = _np.power(u_n,2)

    g_ = r*(gamm-1.)/2.*(Ma**2)

    return T_w + (T_aw - T_w)*u_n - g_*u_n2

def __falk_skan_sys(f, eta, beta):
    """
    For numerical solution of Falkner Skan Equation
    
    f[0] : f
    f[1] : f'
    f[2] : f''
    
    F[0] : f'
    F[1] : f''
    F[2] : f'''
    """
    F = _np.zeros((3))
    
    F[0] = f[1]
    F[1] = f[2]
    F[2] = -f[0]*f[2] - beta*(1.-f[1]*f[1])
    
    return F

def get_m_from_beta(beta):
    return beta/(2.-beta)
     
def get_beta_from_alpha(alpha):
    return alpha*2/_np.pi
    
def get_alpha_from_beta(beta):
    return _np.pi/2.*beta
    
def solve_falk_skan(alpha, f0=0., eta_max=6., eta=None, k_max=100 ):
    """
    From White Visc. Flows
    
    INPUT :: 
    
    alpha : (angle of attack in degrees) := beta*pi/2
    
    
    abeta : pressure gradient term  > 0 favorable, <0 unfavorable
    
           beta = 0, m = 0                    : flat plate
           0 <= beta <= 2, 0 <= m <= infinity : wedge with half angle beta*pi/2
           beta = 1, m = 1                    : plane stagnation point
           beta = 4, m = -2                   : doublet flow near plane wall
           beta = 5, m = -5/3                 : doublet flow near 90 deg corner
           beta = infinity, m = -1            : flow toward point sink (convergent wedge)
           

    f0 : for blowing/suction boundary condition, f0 = 0 for no mass flow

    eta_max : if eta=None, set maximum in non-dimensional units

    eta :  ndarray of spatial points in non-dimensional units
    
    k_max : number of iterations allowed
    
    OUTPUT ::
    
    F : F[0] = f'   : non-dimesional velocity u/u_ref
        F[1] = f''  : non-dimensional shear stress
        F[2] = f''' 
         
    """
    
    def _get_eta_star(fp, eta):
        q = 1. - fp
        
        return _np.trapz(q, x=eta)
    
    def _get_theta_star(fp, eta):
        q = fp*(1.-fp)
        
        return _np.trapz(q, x=eta)
    
    def _get_y_from_eta(eta, m, U, x, nu):
        r = (m+1.)/2.*(U/(nu*x))
        
        return eta*_np.power(r, -0.5)
    
    ##
    print('-'*60)
    print(' FALKNER SKAN ')
    print('-'*60)
    
    alpha_rad = alpha*_np.pi/180.
    
    abeta = get_beta_from_alpha(alpha_rad) #(_np.pi/180.)*2./_np.pi*alpha
    beta_warn = -0.19884
    
    if abeta < beta_warn:
        print('WARNING :: beta {:.4f} is less than or equal to {:.4f}'.format(abeta,beta_warn))
    else:
        print(' beta is {:.4f}'.format(abeta))
        
    m = get_m_from_beta(abeta)
    
    fp0 = 0. # BC
       
    # nondimensional spatial vector
    if eta == None:
        nr_eta = 200
        eta = _np.linspace(0., eta_max, nr_eta)
    
    # limits
    err_co = 1e-6
    k_max = 50
    
    #fpp0_min = -2
    #fpp0_max = 4
    
    fpp0 = _np.zeros(k_max)
    fpp0[0] = 1.5
    fpp0[1] = 1.48
    
    
    err_k = _np.ones(k_max)
    
    # search for BC of f''(0) -->
    kk=0
    print('-'*60)
    print("k\tf\'\'(0)\terr")
    print('-'*60)
    
    while kk < k_max-1:
             
        # boundary conditions
        bc = [ f0, fp0, fpp0[kk] ]
        my_args = (abeta,)
            
        F = odeint(_falk_skan_sys, bc, eta, args=my_args, \
                    #Dfun=None, col_deriv=0, \
                    #full_output=0, \
                    #ml=None, mu=None, \
                    #rtol=None, atol=None, \
                    #tcrit=None, \
                    #h0=0.0, hmax=0.0, hmin=0.0, \
                    #ixpr=0, \
                    #mxstep=0, mxhnil=0, mxordn=12, mxords=5
                    printmessg=False)         
    
        # does fp --> 1 as eta --> infty
        fp_mx = _np.nanmax(F[:,1])
        #fp_mx = F[-1,0]
        fp_mn = _np.nanmin(F[:,1])
        
        #print('...', fp_mn, fp_mx)
        
        err_k[kk] = _np.abs(fp_mx - 1.)
        print('{:d}\t{:.4f}\t{:.2e}'.format(kk,fpp0[kk],err_k[kk]))
        
        if (err_k[kk] <= err_co) and (fp_mx <= 1) and (fp_mn >= 0):
            print('-'*60)
            print('...stopping')
            break
                
        # guess new bc
        # secant
        
        if kk > 0: 
            fpp0[kk+1] = fpp0[kk] - err_k[kk]*(fpp0[kk] - fpp0[kk-1])/ (err_k[kk]-err_k[kk-1])
            
        if fpp0[kk+1] < 0:
            fpp0[kk+1] = .5*(fpp0[kk]-fpp0[kk+1])
        
        kk += 1
     
    eta_star = _get_eta_star(F[:,1], eta)
    theta_star = _get_theta_star(F[:,1], eta)
    
    print('-'*60)
    print(' eta*   : {:.4f}'.format(eta_star))
    print(' theta* : {:.4f}'.format(theta_star))
    print('-'*60) 
     
    return eta, F


def plt_falk_skan(eta, F, fig_sz=[6,6], fig_dpi=200):
    fsx, fsy = fig_sz

    f = _plt.figure(figsize=(fsx, fsy), dpi=fig_dpi)
    
    ax = f.add_subplot(221)
    
    ax.plot(eta, F[:,0])
    ax.set_xlabel('$\eta$')
    ax.set_ylabel('$f$')
    
    ax2 = f.add_subplot(222)
    
    ax2.plot(eta, F[:,1])
    ax2.set_xlabel('$\eta$')
    ax2.set_ylabel('$f\'$')   

    ax3 = f.add_subplot(223)
    
    ax3.plot(eta, F[:,2])
    ax3.set_xlabel('$\eta$')
    ax3.set_ylabel('$f\'\'$')   
    
    return f, ax, ax2, ax3

    
def mu_sutherland(T, T_ref=273.15, S=110.4, mu_ref=1.716e-5):
    """ 
    T_ref  : [K]
    S      : [K]
    mu_ref : [kg/m/s]
    """

    return mu_ref*(T/T_ref)**(3./2.)*(T_ref+S)/(T+S)

def ref_temp_white(T_inf, T_wall, Ma_inf):
    """ reference temperature method by White
    (see Anderson High Temp. Gas Dynamics pp342-343)
    """
    return T_inf*(1.+0.032*Ma_inf**2. + 0.58*(T_wall/T_inf-1))


def cf_lam_blausius(Re):
    """ Skin Friction for Laminar Blausius Boundary Layer
    """
    return 0.664/Re


def cf_turb(Re):
    return 0.0592/(Re**0.2)

def get_tau_cf(rho_inf, u_inf, cf):
    return 0.5*rho_inf*u_inf**2.*cf

def estimate_deltayplus(u_inf, \
                        rho_inf, p_inf, T_inf, \
                        mu_inf, \
                        L_ref, yp_ref, \
                        gamm_=1.4, R=287.,
                        is_comp=False, T_w=300., \
                        is_lam=True, \
                        out_fn=None):
    """ Estimate First Cell Height

    INPUT ::

    U_inf   : free stream velocity [m/s]
    rho_inf : free stream density [kg/m^3]
    p_inf   : free stream pressure [Pa]
    T_inf   : free stream temperature [K]
    mu_inf  : free stream velocity [kg/m/s]
    L_ref   : reference length scale [m]
    yp_ref  : target yplus at wall [--]
    gamm_   : ratio of specific heats [--]
    R       : specific gas constant [J/kg/K]
    is_comp : (bool) is compressible 
    T_w     : wall temperature used in reference temperature method [K]
    is_lam  : (bool) is laminar
    out_fn  : filename to write output to file

    """

    Re_inf = rho_inf*u_inf*L_ref/mu_inf

    if not is_comp:
        # Incompressible
        Re_ = Re_inf
        rho_ = rho_inf
        mu_ = mu_inf

        cmp_str = 'False'
    else:
        # Compressible
        Ma_inf = u_inf/np.sqrt(gamm_*R*T_inf)

    
        T_star = ref_temp_white(T_inf, T_w, Ma_inf)

        mu_star = mu_sutherland(T_inf)    # Sutherlands
        rho_star = p_inf/(R*T_star)
        Re_ = rho_star*u_inf*L_ref/mu_star
        rho_ = rho_star
        mu_ = mu_star

        cmp_str = 'True'

    
    if is_lam:
        # laminar
        cf_ = cf_lam_blausius(Re_)
        lam_str = 'True'

    else:
        # turbulent
        cf_ = cf_turb(Re_)
        lam_str = 'False'


    tau_w = get_tau_cf(rho_, u_inf, cf_)

    u_plus = get_u_star(tau_w, rho_)

    dy = yp_ref*(mu_/rho_)/u_plus

    inp_str = '-'*60 + '\n'
    inp_str += '         INPUT\n'
    inp_str += '-'*60 + '\n'
    inp_str += ' > Is Compressible        : {:s}\n'.format(cmp_str)
    inp_str += ' > Is Laminar             : {:s}\n'.format(lam_str)
    inp_str += ' > Freestream velocity    : {:.2f} [m/s]\n'.format(u_inf)
    inp_str += ' > Freestream density     : {:.4e} [kg/m^3]\n'.format(rho_inf)
    inp_str += ' > Freestream viscosity   : {:.4e} [kg/m/s]\n'.format(mu_inf)
    inp_str += ' > Freestream pressure    : {:.4e} [Pa]\n'.format(p_inf)
    inp_str += ' > Freestream temperature : {:.2f} [K]\n'.format(T_inf)
    inp_str += ' > Reference length       : {:.4e} [m]\n'.format(L_ref)
    inp_str += ' > Reference delta y+     : {:.1f} [--]\n'.format(yp_ref)
    inp_str += '\n\n'

    print(inp_str)



    out_str = '-'*60 + '\n'
    out_str += '          OUTPUT\n'
    out_str += '-'*60 + '\n'
    out_str += ' > Calculated Reynolds Number : {:.4e} [--]\n'.format(Re_)
    out_str += ' > Estimated delta y          : {:.4e} [m]\n'.format(dy)

    
    print(out_str)

    
    

    