#!/usr/bin/env python


# TODO: how handle interface when it could be called w/ or w/out pyCAPS

import os

from .util.env import verifyEnv
from interface import Interface
# TODO: add pyCAPS dir to path? or assume already defined
from .__init__ import ESP_ROOT_DEF # TODO: or seek from os.environ

### GLOBALS

ENV_REQD = ['ESP_ROOT', 'CAPS_ROOT']
AIM_LIST = ['egadsTessAIM', "tetgenAIM", 'aflr2AIM', 'aflr3AIM', 'aflr4AIM', \
            'cart3dAIM', 'su2AIM', 'fun3dAIM', \
           'astrosAIM'] # populate and verify when calling an aim 

# ESP_ROOT/EngSketchPad/{pyESP,pyEGADS,pyOCSM}

### END GLOBALS

if ESP_ROOT_DEF is None:
    try:
        import pyCAPS
    except ImportError as e:
        str_ = 'CAPS_HOME is not defined or pyCAPS install not in PATH or PYTHONPATH'
        raise ValueError(str_)
else:
    os.environ['PYTHONPATH'] += os.path.join(ESP_ROOT_DEF,'EngSketchPad/pyESP')
    import pyCAPS


class CAPS(Interface):
    def __init__(self, reqdinp=None, reqdout=None, reqdinc=None, workdir=None):
        super().__init__('caps', ENV_REQD, reqdinp, reqdout, reqdinc, workdir)
        self.prob = None

    def setup(self):
        self.verifyEnvVar()
        pass

    def init(self, problem, geom_file):
        self.prob = pyCAPS.Problem(problemName=problem, capsFile=geom_file)

    def pre(self, name=None):
        if name is None:
            pass # loop
        else:
            pass # get self.prob.analysis.keys(), do preAIM(key)
            

    def run(self):
        pass # TODO: 

    def post(self):
        pass

    def addAIM(self, aim, label, intent):
        self.prob.analysis.create(aim = aim,
                                name = label,
                                capsIntent = intent)
        
    
    def linkAIMs(self, A, A_out, B, B_in):
        self.prob.analysis[B].input[B_in].link(self.prob.analysis[A].output[A_out])

    def preAIM(self, name):
        self.prob.analysis[name].preAnalysis()

    def postAIM(self, name):
        self.prob.analysis[name].postAnalysis()

    

def newCAPS(prob, csm, **kwargs): # TODO: cleanup kwargs
    C = CAPS(**kwargs)
    C.init(prob, csm)
    return C
    # instantiate interface
    # pass args to setup problem
    # return problem


# shoudl this also derive from Interface() ? would think no as this will be higher up the chain than other tools...
# AND/OR integrate into analysis module 



