#!/usr/bin/env python


# see MIT : https://acdl.mit.edu/ESP

# should this also derive from Interface() ? maybe no as this will be higher up the chain than other tools...

# TODO: wold be nice to pass units of a Parameter() to .csm file but csm doesn't really have dimension per se 
# TODO: add func to bypass CAPS to  run ESP?

from .util.env import verifyEnv

from interface import Interface

import os


ENV_REQD = ['ESP_ARCH', 'ESP_ROOT', \
    'CAPS_GLYPH', \
        'CASROOT', 'CSREV', 'CASARCH', \
            'SLUGS_START', 'ESP_START', 'WV_START' ]

class ESP(Interface):
    def __init__(self, caps_obj, csm_file, reqdinp=None, reqdout=None, reqdinc=None):
        inc = [csm_file]
        if reqdinc is not None:
            inc += reqdinc    
        super().__init__('esp', ENV_REQD, reqdinp=reqdinp, reqdout=reqdout, reqdinc=inc)
        self.caps = caps_obj # TODO: if isintance(super(caps_obj), Interface then ... else ...
        self.setup()

    def setup(self):
        self.verifyEnvVar()
        # TODO: anything else

    def run(self):
        pass

    def post(self):
        pass

    def save(self, dir_, fname, ext):
        self.caps.geometry.save(fname, directory=dir_, extension=ext)

    def update(self, design):
        for pname in design.params:
            if pname in self.caps.geometry.despmtr:
                self.caps.geometry.despmtr[pname] = design.params[pname].val # TODO: units

    def addCAPS(self, cap_prb):
        self.caps = cap_prb

    def getDpmtr(self, name):
        return getattr(self.caps.geometry.despmtr, name)

    def getOpmtr(self, name):
        return getattr(self.caps.geometry.outpmtr, name)

    def getCfgpmtr(self, name):
        return getattr(self.caps.geometry.cfgpmtr, name)

    def setDpmtr(self):
        pass

    def setCfgpmtr(self):
        pass

    def getAllDpmtr(self):
        pass

    def getAllCfgpmtr(self):
        pass

    def getAllOpmtr(self):
        pass


#
def runCSM(csm_file):
    pass



# need to add to PYTHONPATH ESP_ROOT/bin ESP_ROOT/lib, ESP_ROOT/pyESP, Python38/Lib/site-packages
# need to add to PATH   toplevel/Python38 ; toplevel/Python38/Scripts ; toplevel/OpenCASCADE-#/.../bin


# export PATH=$SPATH
# export ESP_ARCH=LINUX64
# export ESP_ROOT=$ESP_ROOT
# export CASROOT=$ScriptDir/OpenCASCADE-7.4.1
# export CARARCH=.
# export CASREV=7.4
# export LD_LIBRARY_PATH=$LDPATH
# export PYTHONINC=$ScriptDir/Python-3.8.9/include/python3.8
# export PYTHONLIB="-L$ScriptDir/Python-3.8.9/lib -lpython3.8"
# export PYTHONPATH=$PPATH
# export CAPS_GLYPH=$ESP_ROOT/src/CAPS/aim/pointwise/glyph
# export UDUNITS2_XML_PATH=$ESP_ROOT/src/CAPS/udunits/udunits2.xml
# export SLUGS_START="firefox $ESP_ROOT/SLUGS/Slugs.html"
# export ESP_START="firefox $ESP_ROOT/ESP/ESP-localhost7681.html"
# export WV_START="firefox $ESP_ROOT/wvClient/wv.html"
