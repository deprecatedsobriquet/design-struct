#!/usr/bin/env python


from ..interface import Interface
#from ..env import verifyEnv

import numpy as np
import matplotlib as mpl
import matplotlib.pyplot as plt


### GLOBALS

REQ_ENV = [] # to sanity check working environment, e.g. A_CODE_HOME, CODE_LIB_PTH, ...
REQ_INC = [] # minimum include files list to call 
REQ_INP = [] # minimum list of inputs to call
REQ_OUT = [] # reqd output list

### END GLOBALS


class CodeA(Interface):
    # prototype interface
    def __init__(self, name, input_d, output_d, **kwargs):
        # other stuff
        #self.tool = name
        #self.inputs = input_d
        #self.outputs = output_d
        super().__init__() # TODO: pass req'd env, inp, out, inc

        self.setup(**kwargs) 
    
    def setup(self, **kwargs):
        # do stuff

        # then
        self.verifyEnvVar()

    def initialize(self, design):
        # pass params from design to code
        # verify design provides all req'd inps
        # TODO: include work path to set env vars?
        pass

    def run(self):
        # call code kick off
        pass

    def post(self, design, fig_dpi=200):
        # stuff to do after

        fig = plt.figure(figsize=[4,4],dpi=fig_dpi)
        ax = fig.add_subplot(111)
        x = np.linspace(0,10, 101)
        y = 1./(x+1e-3)*np.sin(10*x)
        ax.plot(x,y)

        ax.set_xlabel('x')
        ax.set_ylabel('y')

        fig.tight_layout()
        fig.savefig('codeA.png', dpi=fig_dpi)
        

    # def, other needed methods
    