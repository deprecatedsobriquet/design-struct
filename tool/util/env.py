#!/usr/bin/env python



import os
#import sys


### GLOBALS




### END GLOBALS


# todo: validate that for given code, appropriate env vars are set

def verifyEnv(env_reqd): # allow list or dict as input
    seen = set()
    reqd = set()

    # for ea in reqd, add to set

    # get environ
    env_ = os.environ

    # for ea element in env_req_d
    for key in env_reqd:
        reqd.add(key)
        if key in os.environ:
            seen.add(key)

    # compare seen==reqd
    if seen == reqd:
        return True
    else:
        return False
    
