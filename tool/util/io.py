#!/usr/bin/env python

import os

import uuid # don't use uuid1() as it contains system information, uuid4 is random

from datetime import datetime as dt

import numpy as np

import h5py as h5

#from .design import Design

from ..units import unitConvert

##

DATETIME_FMT = '%Y-%m-%d %H:%M:%S %f'

# TODO: make separate h5 module? and/or make shared/ folder and push h5, units, etc to that submodule?

##
def fmtDateTime(dt_=None):
    if dt_ is None:
        dt_ = dt.now()
    return dt_.strftime(DATETIME_FMT)

def strToDT(dt_str):
    return dt.strptime(dt_str, DATETIME_FMT)
##


def genFileName(base, ext, dt=False):
    if dt:
        return '{}_{}.{}'.format(base, fmtDateTime(), ext) # TODO: how handle if dt is datetime obj
    else:
        return '{}.{}'.format(base,ext)







# CAVEAT EMPTOR: it is possible to define other datatypes for H5 but assume numpy will be most portable
def toH5(val): 
    #print('zzz', type(val))
    if type(val) is str:
        return toH5str(val)
    elif type(val) is bool or type(val) is np.bool_:
        return toH5bool(val)
    elif type(val) is int or type(val) is np.int:
        return toH5int(val) # handle if passing numpy datatypes directly
    elif type(val) is float or type(val) is np.float:
        return toH5float(val) # handle if passing numpy datatypes directly
    else:
        str_ = 'unknown type {} in toH5 '.format(type(val))
        raise ValueError(str_)

def toH5str(str_):
    return np.string_(str_)

def toH5bool(bool_):
    return np.bool(bool_)

def toH5float(num):
    return np.float(num)

def toH5int(num):
    return np.int(num)


def fromH5(val):
    if type(val) is np.bytes_: # handle bytecode and convert to str
        return fromH5str(val)
        #print('...', type(tmp), tmp) # DEBUG
    elif type(val) is np.bool_:
        return bool(val)
    else:
        return val[()]

def fromH5str(val):
    return val.decode('ascii')

def fromH5bool(val):
    pass

def fromH5int(val):
    pass


def writeH5Param(fid, id_,  par):
    grp = 'parameters'

    d_ = {'unit' : par.units,
        'imetric' : par.imetric,
        'type' : par.typ_}

    fid[id_][grp][par.name] = toH5float(par.val) #np.float(par.val) # TODO: what precision to write : 8, 16, 32, ...?
    
    for k in d_:
        fid[id_][grp][par.name].attrs[k] = toH5(d_[k]) 

def writeAllH5Params(design, fid):
    id_ = str(design.id)
    for name in design.params:
        writeH5Param(fid, id_, design.params[name])

def writeH5Con(fid, id_, con):
    grp = 'constraints'
    fid[id_][grp][con.cname] = np.float(con.val)

    d_ = {'var' : con.var,
        'relation' : con.relation,
        'units': con.units,
        'met' : con.met}

    for k in d_:
        fid[id_][grp][con.cname].attrs[k] = toH5(d_[k])



def writeAllH5Cons(design, fid):
    id_ = str(design.id)
    for name in design.constraints:
        writeH5Con(fid, id_, design.constraints[name])

def writeAllH5Custom(design, fid):
    grp = 'custom'
    id_ = str(design.id)
    for attr in design.custom:
        fid[id_][grp].attrs[attr] = toH5(design.custom[attr])


def getH5DesignIDs(fid):
    # with file ptr, get top-level groups
    pass

def writeTopAttrs(design, fid):
    id_ = str(design.id)

    # top level attrs in design, add or remove as desired
    d_ = {'creation' : fmtDateTime(design.started),
            'uuid'   : str(design.id),
            'label'  : design.label,
            'project' : design.project,
            'config'  : design.config,
            'closed'  : design.closed,
            'nparams' : design.nparams,
            'ncons'   : design.ncons,
            'nmetrics' : design.nmetrics,
            'lastmod'  : fmtDateTime(design.lastmod),
            'fromdisk' : design.fromdisk,
            'platform' : design.platform,
            'pyver'    : design.pyver,
            'distribution'    : design.distri,
            'dissemination'   : design.dissem,
            'classification'  : design.class_,
            'controlling-org' : design.ctrlorg}

    for k in d_:
        #print(type(d_[k])) DEBUG
        #print(k, d_[k])
        fid[id_].attrs[k] = toH5(d_[k])       

def storeDesignH5(design, fname=None, overwrite=True):
    # would like to package data structure
    # shelve, XML, hdf5 file? , create numpy datastruct and use IO methods from np?

    if fname is None:
        fname = design.genFileName('h5')

    # TODO:, handle multiple designs per file?

    i_new = True
    if os.path.exists(fname) and overwrite is False:
        i_new = False

    # TODO:: handle external links? 

    if overwrite is True:
        with h5.File(fname, 'w') as f: 

            id_ = str(design.id) # convert obj to str

            # TODO: verify if id_ already exists?

              
            f.create_group(id_) # create top-level group to allow multiple designs per file
            # alternatively, make numpy array for parameters across design

            writeTopAttrs(design, f)
                     
            # additional groups to bundle different quantities
            f[id_].create_group('parameters')
            f[id_].create_group('constraints')
            f[id_].create_group('custom') 

            writeAllH5Params(design, f)
            writeAllH5Cons(design, f)
            writeAllH5Custom(design, f)   

            #f[id_].create_group('analyses')
            # f.create_dataset(path, data=np_data)


    else: # append
        with h5.File(fname, 'a') as f: # TODO: handle if file exists

            # TODO: check if summary group exists, 
            # TODO: track_order = True?

            id_ = str(design.id) # convert obj to str
            f.create_group(id_) # create top-level group to allow multiple designs per file
            # alternatively, make numpy array for parameters across design

            writeTopAttrs(design, f)

            grp_lst = ['parameters', 'constraints', 'custom']
            for g in grp_lst:
                f[id_].create_group(g)
            #f[id_].create_group('parameters')
            #f[id_].create_group('constraints')
            #f[id_].create_group('custom') 

            writeAllH5Params(design, f)
            writeAllH5Cons(design, f)
            writeAllH5Custom(design, f)   

            _ = summarizeH5Designs(f, store=True) # when appending save to file

    ##


def readTopLevelAttrs(fid, id_, design):
    # read top-level attrs
    #   map H5 key to obj,
    #   skip fromdisk
    map_ = {'creation' : 'started',
            'uuid'    : 'id',
            'label'   : 'label',
            'project' : 'project',
            'config'  : 'config',
            'closed'  : 'closed',
            'nparams' : 'nparams',
            'ncons'   : 'ncons',
            'lastmod' : 'lastmod',
            'platform' : 'platform',
            'pyver'    : 'pyver',
            'distribution'    : 'distri',
            'dissemination'   : 'dissem',
            'classification'  : 'class_',
            'controlling-org' : 'ctrlorg'
            }        
    
    date_attr = ['lastmod', 'creation']  # TODO: convert date str to dt obj

    # use getattr, setattr to map H5 id to design object
    for k in map_:
        tmp = fromH5(fid[id_].attrs[k]) #[()]
        #print(' type ', type(tmp), tmp) # DEBU
        
        if k in date_attr:
            #print('zz',k, type(val))
            val = strToDT(tmp)
        elif k == 'uuid':
            val = uuid.UUID(tmp)
        else:
            val = tmp
        setattr(design, map_[k], val) # TODO need something like fromH5 to convert ?



def readH5Con(fid, name, design): # design):
    # TODO: decouple from design obj? perhaps as dict
    id_ = str(design.id)
    grp = 'constraints'
    val      = fromH5(fid[id_][grp][name])
    var      = fromH5(fid[id_][grp][name].attrs['var'])
    relation = fromH5(fid[id_][grp][name].attrs['relation'])
    units    = fromH5(fid[id_][grp][name].attrs['units'])
    #met = fid[id_]['constraints'][name].attrs['met'] # call validate cons after read
    design.addDesCons(name, var, relation, val, units)


def readAllH5Cons(fid, design):

    # get all names under constraints
    # 
    id_ = str(design.id)
    c_lst = fid[id_]['constraints'].keys()

    for cname in c_lst:
        readH5Con(fid, cname, design)


def readH5Param(fid, name, design):
    id_ = str(design.id)

    grp = 'parameters'
    val     = fromH5(fid[id_][grp][name])[()]
    units   = fromH5(fid[id_][grp][name].attrs['unit'])
    imetric = fromH5(fid[id_][grp][name].attrs['imetric'])
    typ_    = fromH5(fid[id_][grp][name].attrs['type'])
    
    #print(val, units, imetric, typ_) #DEBUG
    design.addDesParam(typ_, name, val, units)
    if imetric:
        design.addMetric(name)

def readAllH5Params(fid, design):
    id_ = str(design.id)

    for pname in fid[id_]['parameters'].keys():
        readH5Param(fid, pname, design)


def readH5Custom(fid, name, design):
    id_ = str(design.id)
    val = fromH5(fid[id_]['custom'].attrs[name]) # TODO: conversion
    design.addCustAttr(name, val)

def readAllH5Custom(fid, design):
    id_ = str(design.id)
    for k in fid[id_]['custom'].keys():
        readH5Custom(fid, k, design)


def getAllH5Keys(fid):
    # return top-level groups
    k_iter = [kk for kk in fid.keys()]

    return k_iter

def enumH5Keys(k_lst):
    # TODO: func to walk tree ?
    d_ = {}
    for i, k in enumerate(k_lst):
        d_[i] = k_lst[k]
    return d_

def procDesignH5(fid, id_):
    pass

def loadDesignH5(design, fname, id_=None): # move to 'shared' module

    if not os.path.isfile(fname):
        str_ = 'specified HDF file {} does not exist'.format(fname)
        raise IOError(str_)

    with h5.File(fname, 'r') as f:
        id_lst = getH5DesignIDs(getAllH5Keys(f))

        #k_lst = getAllH5Keys(f)
        print(id_lst)
        if id_ is None:
            id_ = id_lst[0] # TODO: warn
        else:
            if id_ not in id_lst: # TODO: make warning not halt?
                str_ = ' WARNING : id {} not in specified file {}'.format(id_, fname)
                raise ValueError(str_)

        print(' ... reading data from {}'.format(id_))

        design.fromdisk = True # set this first

        readTopLevelAttrs(f, id_, design)
        readAllH5Params(f, design)
        readAllH5Cons(f, design)
        readAllH5Custom(f, design)
        

def getH5DesignIDs(keys):
    id_lst = []

    for k in keys:
        if k != 'summary':
            id_lst.append(k)

    return id_lst


# TODO:: summarize multiple designs in single HDF file as table (pandas or numpy) ?
def summarizeH5Designs(fid, store=False): # TODO: to np for other usage (e.g. surrogates)

    uniq_params = []

    units_lst = []
    nr_uniq_params = 0

    #d_lst = [] # TODO: rm?

    # get list of designs by uuid --> nr_designs
    
    id_lst = getH5DesignIDs(getAllH5Keys(fid)) 
    nr_designs = len(id_lst)

    # for ea. design, get list of parameters
    for id_ in id_lst:
        #d_lst.append(Design('dmy')) # TODO: decouple from Design class, refactor read/write to handle dicts as intermediary?  
        #readTopLevelAttrs(fid, id_, d_lst[-1])
        ##readAllH5Params(fid, d_lst[-1]) # assume d_lst[-1].id is set      
        #readAllH5Cons(fid, d_lst[-1])
        #readAllH5Custom(fid, d_lst[-1])    
        #    
        for pname in fid[id_]['parameters'].keys():
            if pname not in uniq_params:
                units_lst.append(fromH5(fid[id_]['parameters'][pname].attrs['unit']))
                uniq_params.append(pname)

    # TODO: verify len(units_lst) == len(uniq_params)

    # once set of param names generated, 
    nr_uniq_params = len(uniq_params)
    
    data_tbl = np.zeros((nr_designs, nr_uniq_params))
    
    for ii, id_ in enumerate(id_lst):
        for jj, pname in enumerate(uniq_params):
            if pname in fid[id_]['parameters']:
                #scl = 1.0
                scl = unitConvert(units_lst[jj], fromH5(fid[id_]['parameters'][pname].attrs['unit']))     # TODO: need to verify correct units , ordering problem?
                data_tbl[ii,jj] = scl*fromH5(fid[id_]['parameters'][pname])
            else:
                data_tbl[ii,jj] = np.nan
                
    if store:
        # store var names
        fid.create_dataset('summary', data=data_tbl, track_order=True, dtype='f') 
        for ii, v in enumerate(uniq_params):
            fid['summary'].attrs[v] = toH5(units_lst[ii]) # when group created, need to set track_order=True, to make sure we are in a consistent order

    return [data_tbl, uniq_params, units_lst]

