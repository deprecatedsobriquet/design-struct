#!/usr/bin/env python




#from .__init__ import DEFAULT_ORG


### GLOBALS

VALID_DIST_LEGACY = ['NA', 'A', 'B', 'C', 'D', 'F'] # TODO
VALID_DIST = ['NA', 'SBIR', 'IRAD']  # TODO: remove?
VALID_DISSEM = ['NA'] # https://www.archives.gov/cui/registry/limited-dissemination
VALID_CLASS = ['NA', 'U', 'CUI', 'CONFIDENTIAL'] # TODO
VALID_IT__ = ['NA']  # TODO

### END GLOBALS



def verifyDist(dist):
    if dist not in VALID_DIST or dist not in VALID_DIST_LEGACY:
        return False
    else:
        return True

def verifyDissem(dissem):
    if dissem not in VALID_DISSEM:
        return False
    else:
        return True

def verifyClass(class_):
    if class_ not in VALID_CLASS:
        return False
    else:
        return True

def verifyCtrlOrg(org):
    if org is None:
        return False
    else:
        return True