#!/usr/bin/env python

from .units import unitConvert, VALID_UNITS
# add ctrl to param struct

class Parameter(object):
    # TODO: add generic methods to handle additional metadata, need to figure out scalable IO
    def __init__(self, typ_, name, val=None, units=None):
        self.typ_ = typ_
        self.name = name
        self.val = val
        self.units = units
        self.imetric = False
        self.attr = {}
        self.validateUnits()

    def validateUnits(self):
        if self.units not in VALID_UNITS:
            str_ = 'invalid units {} specified for parameter {}'.format(self.units, self.name)
            raise ValueError(str_)

    def update(self, val): # todo: handle units?
        # warn? or track e.g. Design.lastmod
        self.val = val

    def __repr__(self):
        pass

    def __str__(self):
        return 'PARAM : KIND {} @NAME {} {} [{}] imetric={}'.format(self.typ_, self.name, self.val, self.units,self.imetric)

    def ckUnits(self, other):
        if self.units == other.units:
            return True
        else:
            return False

    def __add__(self, other):  # take first param as referrent for determining operating units
        # need to validate units
        if self.ckUnits(other) is True:
            # todo: warn?
            return self.val + other.val
        else:
            scl = unitConvert(self.units, other.units)
            return self.val + scl*other.val

    def __iadd__(self, const):
        return self.val + const

    def __sub__(self, other):
        if self.ckUnits(other) is True:
            # todo: warn?
            return self.val - other.val
        else:
            scl = unitConvert(self.units, other.units)
            return self.val - scl*other.val


    def __mul__(self, other):
        if self.ckUnits(other) is True:
            # todo: warn?
            return self.val + other.val
        else:
            scl = unitConvert(self.units, other.units)
            return self.val * scl*other.val



    def __eq__(self, other):
        # compare param typ_ ?
        pass

    def asJSON(self):
        return [self.val, self.units, self.imetric]


        