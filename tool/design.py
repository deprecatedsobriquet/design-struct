#!/usr/bin/env python


# prototyping data structures


# import analysis as analysis, 
#   if this is superclass for specific interfaces to other tools, 
#   perhaps just import those other analysis subclasses
#   BUT analysis should perhaps encapsulate a design(s) so maybe best not to import at all


# TODO: is it valuable to have XML IO for data?

# TODO: use isinstance for error handling

import uuid # don't use uuid1() as it contains system information, uuid4 is random

#import numpy as np

import json
import xml # TODO:

from datetime import datetime as dt

#import os  # TODO:
import sys

from .parameter import Parameter
from .constraint import Constraint

from units import unitConvert

from .util.io import fmtDateTime, \
    storeDesignH5, loadDesignH5, \
        genFileName

from .__init__ import DEFAULT_ORG # TODO: or pass from Analysis, want to reduce dependency tree

from .util.distribution import verifyCtrlOrg, verifyDist, verifyDissem, verifyClass, \
    VALID_DIST_LEGACY, VALID_DIST, VALID_DISSEM, VALID_CLASS

# TODO: add workpath

class Design(object):
    """ data structure for holding design parameters, constraints
    """
    def __init__(self, label, project, config, fromdisk=False, \
                distri=None, dissem=None, class_=None, ctrlorg=None):
        # enable init of parameters through dict?
        self.started = dt.now() # TODO:, name
        self.lastmod = self.started # datetime of update, should we save type of modification for traciblity?
        self.id = uuid.uuid4()
        #
        self.label = label
        self.project = project
        self.config = config
        #
        self.closed = False
        self.params = None
        self.nparams = 0
        self.constraints = None
        self.ncons = 0
        self.metrics = None
        self.nmetrics = 0
        #
        self.analysis = None
        #
        self.fromdisk = fromdisk
        #
        self.platform = sys.platform # TODO: get from analysis
        self.pyver = sys.version # TODO: get from analysis
        #
        self.distri = distri # (LEGACY) TODO: add other controls ?? keep in custom grp attrs ??
        self.dissem = dissem # TODO
        self.class_ = class_ # TODO
        self.ctrlorg = ctrlorg
        self.verifyControls()
        #
        self.custom = {} # for custom attr


    def updateModTime(self): 
        self.lastmod = dt.now()

    def verifyControls(self):
        if not verifyDist(self.distri):
            self.distri = 'NA'
        if not verifyDissem(self.dissem):
            self.dissem = 'NA'
        if not verifyClass(self.class_):
            self.class_ = 'NA'
        if not verifyCtrlOrg(self.ctrlorg):
            self.ctrlorg = DEFAULT_ORG
    #

    def getNumParams(self):
        if self.params is not None:
            self.nparams = len(self.params)

    def addDesParam(self, typ_, name, val=None, units=None):
        if self.params is None:
            self.params = {}
        
        if name in self.params:
            pass
            # warn or halt self.parameters[name].update(val)
        else: # should genereate uuid for param?
            self.params[name] = Parameter(typ_, name, val, units) # can init 
            self.updateModTime()

        self.getNumParams()

    def updateDesParam(self, name, val):
        self.params[name].update(val)
        self.updateModTime()

    def rmDesParam(self, name): # is this needed, 
        # warn
        if name in self.params:
            self.params.pop(name) 
            self.updateModTime()
        else:
            pass # warn RaiseKeyError or warn nicely

        self.getNumParams()

    def desParamFromCSM(self, fname):
        pass # TODO

    def desParamFromTXT(self, fname): # TODO: handle JSON, &c

        with open(fname, 'r') as f:
            for ln in f.readlines:
                typ_, name, val, units = ln.rstrip().split()
                self.addDesParam(typ_, name, val, units)

    def desParamFromJSON(self, fname):
        pass

    def cpParams(self, other):
        for name in self.params:
            p = self.params[name]
            other.addDesParam(p.typ_, p.name, p.val, p.units )

    def lstParams(self):
        if self.params is None:
            return # TODO:: warn nicely

        for name in self.params:
            p = self.params[name]
            print(p.typ_, name, p.val, p.units) # TODO: just print(p) ?


    #

    def getNumMetrics(self):
        if self.metrics is not None:
            self.nmetrics = len(self.metrics)

    def addMetric(self, name):
        if self.metrics is None:
            self.metrics = []
  
        if name in self.params:
            self.params[name].imetric = True
            self.updateModTime()
            if name not in self.metrics:
                self.metrics.append(name)
        else:
            # warn or error?
            pass
        # 
        self.getNumMetrics()
        
    def rmMetric(self, name):
        if name in self.metrics:
            self.params[name].imetric = False
            self.metrics.pop(name)
            self.updateModTime()
        else:
            pass # warn or error?

        self.getNumMetrics()

    def cpMetrics(self, other):
        for m in self.metrics:
            other.addMetric(m)

    def lstMetrics(self): # TODO: integrate logging instead or in addition to?
        if self.metrics is None:
            return # TODO: warn nicely?

        for name in self.metrics:
            print(name)
            
    #

    def addCustAttr(self, name, val):
        if self.custom is None:
            self.custom = {}

        if name not in self.custom:
            self.custom[name] = val
        else:
            pass # TODO : warn?
        
    def rmCustAttr(self, name):
        if self.custom is not None:
            if name in self.custom:
                self.custom.pop(name)

    def custAttrFromFile(self, fname):
        pass

    def cpCustAttrs(self, other):
        pass # TODO

    def lstCustAttr(self):
        for k in self.custom:
            print(k, self.custom[k])

    #
    def asDict(self): # TODO: flag to include imetric=False cases
        # build dict
        json_d = {'label' : self.label,
                'id' : str(self.id),
                'parameters' : {},
                'constraints' : {} }

        # { 'parameters' : {'type' : {'param' : [val, units, ismetric?] } }, 
        #   'constraints' : {'param' : {'con : [relation, val, units, ismet?]} },
        # }

        for pkey in self.params:
            p = self.params[pkey]

            if p.imetric:
                if p.typ_ not in json_d['parameters']:
                    json_d['parameters'][p.typ_] = {}
                
                json_d['parameters'][p.typ_][p.name] = p.asJSON() #[p.val, p.units, p.imetric]

        for ckey in self.constraints:
            c = self.constraints[ckey]

            if self.params[c.var].imetric:
                if c.var not in json_d['constraints']:
                    json_d['constraints'][c.var] = {}

                json_d['constraints'][c.var][c.cname] = c.asJSON() #[c.relation, c.val, c.units, c.met]

        return json_d


    def getFileName(self, ext): # TODO: add datestamp
        return genFileName(self.label, ext) #'{}.{}'.format(self.label,ext)

    def reportTXT(self, fname=None):  # TODO: flag to include imetric=False cases
        
        if fname is None:
            fname = self.getFileName('txt')

        with open(fname, 'w') as f:
            f.write('label : {}\n'.format(self.label))
            f.write('uuid  : {}\n'.format(self.id))

            f.write('\nparameters\n')
            for pkey in self.params:
                par = self.params[pkey] 
                if par.imetric:
                    f.write('{}\n'.format(str(par)))

            f.write('\nconstraints\n')
            for ckey in self.constraints:
                con = self.constraints[ckey]
                if self.params[con.var].imetric:
                    f.write('{}\n'.format(str(con)))

    def reportXML(self, fname=None): # TODO: flag to include imetric=False cases
        pass

    def reportJSON(self, fname=None, indent=3):

        if fname is None:
            fname = self.getFileName('json')
            
        with open(fname, 'w') as f:
            f.write(json.dumps(self.asDict(), indent=indent)) #TODO: save old?

    #

    def getNumCons(self):
        if self.constraints is not None:
            self.ncons = len(self.constraints)

    def addDesCons(self, cname, var, relation, val, units):
        if self.constraints is None:
            self.constraints = {}
        
        if cname in self.constraints:
            pass
            # warn or halt self.constraints[name].update(val)
        else: # should genereate uuid for param?
            self.constraints[cname] = Constraint(cname, var, relation, val, units)
            self.updateModTime()

        self.getNumCons()
        self.validateDesCons()

    def desConFromFile(self, fname):
        pass

    def rmDesCons(self,name):
        self.params.pop(name)
        self.getNumCons()

    def cpConstraints(self, other):
        for k in self.constraints:
            c = self.constraints[k]
            other.addDesCons(k, c.var, c.relation, c.val, c.units)

    def lstDesCons(self):
        if self.constraints is None:
            return # TODO: warn nicely

        for name in self.constraints:
            con = self.constraints[name]
            print(con.cname, con.var, con.relation, con.val, con.units)

    def validateDesCons(self):
        for c in self.constraints:
            con = self.constraints[c]
            con.met = con.isMet(self.params[con.var])

    def checkClosure(self):
        self.validateDesCons()

        for c in self.constraints:
            con = self.constraints[c]
            # on first constraint not met, break 
            if con.met is False:
                self.closed = False
                return 
        
        self.closed = True

    def isClosed(self):
        # on init, set self.closed = None as verification check
        if self.closed is True:
            return True
        else:
            return False

    ##

    def storeDesign(self, fname=None, overwrite=True): # move to 'shared' module
        storeDesignH5(self, fname=fname, overwrite=overwrite)

    def loadDesign(self, fname, id_=None):
        loadDesignH5(self, fname, id_=id_)        

    ##

    def addAnalysis(self, lbl, analysis):
        # want key to map to analysis object, store as dict?
        # desirable to have multiple analyses?
        # lastmod?
        pass

    def rmAnalysis(self):
        # lastmod?
        pass

    #
    def lastUpdated(self):
        # get datetime str and self.lastmod = dt_str, want to track changes, perhaps just leverage logging?
        pass

    def __eq__(self, other):
        if self.id == other.id: # for use case of comparing different files on disk
            return True
        else:
            return False
        # compare if Designs have same metrics, analyses, parameters?
        # how report differences across metrics, analyses, parameters -- separate methods?        

    def __contains__(self, name):
        # must return bool
        if name in self.params:
            return True
        else:
            return False


    def __str__(self):
        return 'label : {},\nid : {}'.format(self.label, str(self.id))

    def __repr__(self):
        # use for reporting?
        pass

    def __len__(self):
        # get number of parameters? or return tuple of (nParams, nCons, nMetrics)
        return (self.nparams, self.ncons, self.nmetrics)

    def __call__(self):
        # 
        self.validateDesCons()

    def isEmpty(self):
        if sum(*len(self)) > 0:
            return False
        else:
            return True







