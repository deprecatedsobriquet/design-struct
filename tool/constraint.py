#!/usr/bin/env python

VALID_RELATIONS = ['le', 'lt', 'ge', 'gt', 'eq'] # TODO: =, <=, < ...

from .units import unitConvert, VALID_UNITS
from .util.distribution import VALID_DIST_LEGACY, VALID_DIST, VALID_DISSEM, VALID_CLASS, VALID_IT__
from .__init__ import DEFAULT_ORG

class Constraint(object):
    
    # derive from parameter? is this really needed?
    
    def __init__(self, cname, var, relation, val, units=None):
        self.cname = cname
        self.var = var
        self.relation = relation
        self.val = val
        self.units = units
        self.met = False
        self.validateUnits()
        self.validateRelation()

    def validateUnits(self):
        if self.units not in VALID_UNITS:
            str_ = 'invalid units {} specified for constraint {}'.format(self.units, self.cname)
            raise ValueError(str_)

    def validateRelation(self):
        if self.relation not in VALID_RELATIONS:
            str_ = 'self.relation={} is not in VALID_RELATIONS'.format(self.relation)
            raise ValueError(str_)

    # add validation method here?
    def isMet(self, param, eps=1e-6):
        # for constraint with label cname
        #   is p.val relation self.val ?

        # TODO: ensure self.units == param.units, else scale

        if self.relation == 'gt':
            return param.val > self.val
        elif self.relation == 'ge':
            return param.val >= self.val
        elif self.relation == 'lt':
            return param.val < self.val
        elif self.relation == 'le':
            return param.val <= self.val
        elif self.relation == 'eq':
            return abs(param.val - self.val) < eps

        return False # assume no otherwise
        # 

    def __str__(self):
        return 'CON {} : @VAR {} {}? {:.6e} [{}] ismet={}'.format(self.cname, self.var, self.relation, self.val, self.units, self.met)


    def asJSON(self):
         return [self.relation, self.val, self.units, self.met]





#class Requirement(Constraint): normal constraint val should map to thr..., need method to convert constraint into Requirement?
#    TODO: thr and obj should be Parameter Objects
#    def __init__(self): # TODO : pass in stuff (*args)
#        # TODO: pass self.super().__init__(*args)
#        self.thr = None
#        self.thr_c = None # str
#        self.obj = None   #
#        self.obj_c = None
#
#    def marginTHR(self, param):
#        # TODO: units
#        return param-self.thr
#
#    def marginTHRPercent(self, param):
#        return self.margin(param)/self.thr*100.



