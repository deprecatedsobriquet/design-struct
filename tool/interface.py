#!/usr/bin/env python




from .util.env import verifyEnv

class Interface(object):

    def __init__(self, tool_, reqdenv=None, reqdinp=None, reqdout=None, reqdinc=None, workdir=None):
        self.tool = tool_    # todo : name for tool
        self.reqdenv = reqdenv # todo : dict?
        self.reqdinp = reqdinp # todo: data struct, dict? kev: val
        self.reqdout = reqdout # todo : data struct
        self.reqdinc = reqdinc # TODO: include files
        self.envvalid = False
        self.workdir = workdir

    def verifyEnvVar(self):
        self.envvalid = verifyEnv(self.reqdenv) 

    def verifyInp(self):
        pass

    def verifyOut(self):
        pass

    def verifyInc(self):
        pass

    def verifyPath(self, var):
        # check if path actually exists
        pass

    def verifyInp(self, inp_d):
        pass

    def verifyOut(self, out_d):
        pass

    def verifyInc(self, inc_d):
        pass
