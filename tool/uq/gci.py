#!/usr/bin/env python

import numpy as _np

def GCI(f2, f1, r=1.5, p=2., Fs=3.):
    """ Grid Convergence Index

    Derived from Richardson Extrapolation

    Ref : Roache, Quantification of Uncertainty in Computational Fluid Dynamics
    Annu. Rev. Fluid Mech. 1997

    f1, f2 : a flow property on two different grids 1,2 (2 assumed finer than 1)

    r : refinement factor
    p : order of accuracy of solution
    Fs : safety factor, assumed = 3, (see Baurle)

    """
    return Fs*_np.abs(f1-f2)/(_np.power(r,p)-1.)