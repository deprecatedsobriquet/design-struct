#!/usr/bin/env python

# TODO: handle combination of units e.g. ft-lbf/s

# TODO: check pint, python-units, units

# TODO: use pyCAPS built-in

ENV_REQD = [ 'UDUNIT_HOME', 'UDUNIT_LIB' ]
# LD_LIBRARY_PATH +=UDUNIT_HOME/lib

from numpy import pi, NaN
from cfunits import Units # 

VALID_UNITS = ['in', 'ft', \
                'm', 'mm', 'cm', \
                'km', 'mi', 'nmi', \
                's', 'ms', \
                  'rad', 'deg', \
                'lbm', 'kg', 
                'lbf', 'N',
                'J', 'Btu', \
                'psi', 'psf', 'Pa', \
                'NA']

UNIT_MAP = {
        'in2m' : 0.0254,
        'm2in' : 1./0.0254,
        'm2ft' : 3.2808399,
        'ft2m' : 1./3.2808399,
        'ft2in' : 12,
        'in2ft' : 1./12.,
        's2ms' : 1000,
        'ms2s' : 1./1000. ,
        'in2cm' : 2.54,
        'cm2in' : 1./2.54, 
        'kg2lbm' : 2.20462262,
        'lbm2kg' : 1./2.20462262,
        'deg2rad' : pi/180.,
        'rad2deg' : 180./pi,
        'lbf2N' : 4.44822162,
        'N2lbf' : 1./4.44822162,
        'Btu2J' : 1055.05585,  
        'J2Btu' : 1./1055.05585, 
        'psi2Pa' : 101325./14.7,
        'Pa2psi' : 14.7/101325.,
        'psi2psf' : 144.,
        'psf2psi' : 1./144.,
        'psf2Pa' : 6894.75729,
        'Pa2psf' : 1./6894.75729,
        'mi2km' : 1.609344,
        'km2mi' : 1./1.609344,
        'km2nmi' : 0.86897624/1.609344, # TODO: check me
        'nmi2km' : 1.609344/0.86897624, # TODO: check me
        'mi2ft' : 5280.,
        'ft2mi' : 1./5280.,
        'mi2nmi' : 0.86897624,
        'nmi2mi' : 1./0.86897624
    }

#  but there is already a library for this I think 
def unitConvert(a,b):
    # return multiplier to make b match  a
    key = '{}2{}'.format(b,a)

    if 'NA' in a or 'NA' in b or a == b:
        return 1.0

    if a not in VALID_UNITS or b not in VALID_UNITS:
        str_ = 'either {} or {} not in VALID_UNITS'.format(a,b)
        raise ValueError(str_)
    
    return UNIT_MAP[key] # todo error handling memo



