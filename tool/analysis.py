#!/usr/bin/env python

import uuid

import sys

from datetime import datetime as dt

from .__init__ import __version__ as TOOL_VER
from .task import Task
from .util.io import DATETIME_FMT, fmtDateTime, strToDT
#import .caps

# TODO: import design? or use __init__.py to put design and analysis in same level of namespace?
# TODO: should add/rm task apply across all Analysis or per design? perhaps 1 analysis/design?
# TODO: ??? just use pycaps to add pycaps analysis to problem?, this file's Analysis maps to Problem level in pyCAPS land

### GLOBALS

DEF_WORK_PATH = '.' # TODO: handlers to update/modify and pass to different interfaces

### END GLOBALS

class Analysis(object):
    def __init__(self, name, capobj=None, workdir=DEF_WORK_PATH):
        self.name = name
        self.capobj = capobj # TODO: need methods to work caps Problem
        self.workdir = workdir
        # TODO: allow these to be defined on init from dict(s)?
        self.tasks = None # dict struct to store 'label' : pointer to  interface to other tool
        self.task_ord = None # list of tasks to execute
        #
        #
        self.platform = sys.platform # how to handle if re-run and this changes? # push to Analysis class and pull attr to this class?
        self.pyver = sys.version # see self.platform
        self.version =  TOOL_VER # pass to design as cust attr?
    #
    def addTask(self, label, task_api, depends=None): # make task an object, need to allow relpath ptr to where analysis takes place
        if self.tasks is None:
            self.tasks = {}

        self.tasks[label] = Task(label,task_api,depends=depends)

    def rmTask(self, label):
        # TODO: update in self.tasks
        # TODO: update in task_ord
        pass

    def detTaskOrd(self):
        # TODO: if dependencies defined, compute order assuming directed acyclic graph, see https://docs.python.org/3/library/graphlib.html ? but how to handle coupled tasks?
        pass

    def setTaskOrd(self, tsk_lst): # TODO: need ordered list
        self.task_ord = tsk_lst # TODO: append

    def runTask(self, name):
        if name in self.tasks:
            self.tasks[name].run()  # need to pass design?
            self.tasks[name].post() # need to pass design?

            # if self.tasks[name].iscoupled, for c in self.tasks[name].couplings: run ...
        # else err?

    def run(self, tsk_lst=None): # can bypass self.task_ord
        if tsk_lst is None: # do all
            tasks_ = tsk_lst
        else:
            tasks_ = self.task_ord
        
        for name in tasks_:
            self.runTask(name)

        # else iterate over user specified list


    def addDesign(self): # how deal with DoE
        pass

    def rmDesign(self):
        pass

    def analyzeDesign(self):
        pass

    def analyzeAll(self):
        pass
        # for d in self.designs: ...

    # ? for each design, run or have only one design / analysis