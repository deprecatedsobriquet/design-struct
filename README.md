# README

## About
- prototyping design tools


## Pre-requisites
- python libs
-- see requirements.txt
-- method: ```make install-req```
- for ESP/CAPS (TODO: spell out) and consequently pyCAPS
-- from: https://acdl.mit.edu/ESP
-- method: see install scripts
--  NOTE: there exists pyCAPS module in PYPI but it is not the pyCAPS you are looking for
- UDUNIT library
-- from: https://www.unidata.ucar.edu/software/udunits/
-- compile (```./configure --<opts>``` ; ```make``` ; ```make install```) and add <installdir>/lib to LD_LIBRARY_PATH


## Optional
- to generate requirements (developer) install pipreqs via ```pip install pipreqs``` or similar 
- 

## Installation
from directory contain tool subdir
``` make install-user``` OR  ```pip install -e . --user```

## Documentation

- attempting to use sphynx, not sure if better than doxygen for purposes here but should talk to JB
- ```make doc```


## Misc Notes


### HDF

to view design data structure, can use HDF library interface for various languages (C, Fortran, Py) or use hdfview GUI tool or use matlab (.mat files are very close to HDF)

-- tested with HDFViewer 3.1.0 on windows

