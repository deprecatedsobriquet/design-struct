#!/usr/bin/env python

from setuptools import setup

import os

from tool.__init__ import __version__ 

__author__ = 'dmy'
__email__ = 'dmy@dmy.com'

def read(fname):
      return open(os.path.join(os.path.dirname(__file__), fname) ).read()

# https://pypi.org/pypi?%3Aaction=list_classifiers

setup(name='tool',
      author=__author__,
      author_email=__email__,
      description = ('stuff', 'foo', 'bar'),
      license=read('LICENSE.md'),
      keywords='stuff',
      url = 'https://something.com',
      long_description=read('README.md'),
      version=__version__,
      classifiers=['Development Status :: 1 - Planning',
                  'Intended Audience :: Science/Research',
                  'Natural Language :: English',
                  'Operating System :: Unix',
                  'Programming Language :: C',
                  'Programming Language :: Python',
                  'Programming Language :: Unix Shell'],
      options={"bdist_wheel" : {"universal" : False}},
      packages=['tool', \
                'tool.codeA',\
                'tool.mdao',\
                'tool.mesh',\
                'tool.cfd',\
                'tool.prop',\
                'tool.traj',\
                'tool.structures',
                'tool.render'])


