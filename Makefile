


PYEXE=python3
PIPEXE=pip3
PREQS=~/.local/bin/pipreqs

banner :
	-figlet -c "A Cool Tool" > banner.txt

clean :
	-find . -name *.pyc -delete
	-find . -name __pycache__ -exec rm -rv {} \;


install-req :
	-${PIPEXE} install -r requirements.txt

install-user : 
	-${PIPEXE} install -e . --user 

gen-req :
	-${PREQS} --force --use-local

# TODO: get datetime and add to bundle label
bundle : clean
	${PYEXE} setup.py sdist 

bundle-all : clean
	${PYEXE} setup.py sdist --formats=gztar,zip

doc :
	-cd doc; make 
# TODO:  make pdf

