#!/usr/bin/env python

import os

from tool import ESP_ROOT_DEF # for some top-level globals

from tool.design import Design 
#from tool.analysis import *
from tool.shared import DATETIME_FMT, fmtDateTime, strToDT
from tool.units import *

#import tool.caps



#if 'PYTHONPATH' not in os.environ:
#    os.environ['PYTHONPATH'] = ""



#os.environ['PYTHONPATH'] += '/home/rileylp/ESP120/EngSketchPad/lib:/home/rileylp/ESP120/EngSketchPad/pyESP:/home/rileylp/ESP120/Python-3.8.9/lib/python3.8/site-packages:'

#print(os.environ['PYTHONPATH'])

#caps_pth = os.path.join(ESP_ROOT_DEF, 'EngSketchPad/pyESP/pyCAPS')
#
#os.environ['PYTHONPATH'] += caps_pth

#print(os.environ['PYTHONPATH'])

#import pyCAPS

if __name__ == '__main__':
    print('-'*30)
    print(' > check env')
    print(ESP_ROOT_DEF)




    PRJ_NAME = 'Brainstorm'

    print('-'*10)
    print(' > design instance test')

    d = Design('Test0', PRJ_NAME, 'v1')
    print('design id : {} '.format(d.id))
    print(d.label)

    print('-'*10)
    d.addDesParam('geom', 'bodyLen', 12, 'ft')
    d.addDesParam('geom', 'bodyWdt', 24, 'in')
    d.addDesParam('geom', 'bodyHt', 4, 'ft')
    d.addDesParam('geom', 'noseRad', 3, 'in')
    d.addDesParam('prop', 'phiCruise', 0.8, 'NA')
    d.lstParams()

    print('nr params : {}'.format(d.nparams))

    print('-'*10)
    d.addDesCons('maxLen', 'bodyLen', 'le', 25, 'ft') # todo, specify units?
    d.addDesCons('minLen', 'bodyLen', 'gt', 5, 'ft')
    d.addDesCons('maxHt', 'bodyHt', 'lt', 10, 'ft')
    d.addDesCons('minHt', 'bodyHt', 'gt', 0.5, 'ft')
    d.validateDesCons()
    d.lstDesCons()

    print('-'*10)
    c_mxlen = d.constraints['maxLen']
    print(c_mxlen)

    print('-'*10)
    d.addMetric('bodyLen') # for interface to other reporting
    d.addMetric('noseRad')
    d.addMetric('phiCruise')
    d.lstMetrics()


    print('-'*10)
    p_bdyLen = d.params['bodyLen']
    p_bdyWdt = d.params['bodyWdt']
    print(p_bdyLen)

    res = p_bdyLen + p_bdyWdt
    print('    sum of dims {:.3f} {}'.format( res, p_bdyLen.units))
    print('product of dims {:.3f} {}^2'.format(p_bdyLen*p_bdyWdt, p_bdyLen.units))

    print('-'*10)
    d.addCustAttr('testattr', -1)
    d.addCustAttr('Q1', 'what is maximum LoD?')
    d.addCustAttr('Q2', 'what is min LoD?')
    d.lstCustAttr()


    print('-'*10)
    d.checkClosure()
    print('design is closed? : ', d.isClosed())

    print('-'*10)
    print(' > env test')
    print(d.platform)
    print(d.pyver)

    print('-'*10)
    print(' > test design write to file')
    print(' ... writing to file')
    d.storeDesign('test.h5')
    d.reportJSON('test.json')
    d.reportTXT('test.txt')

    print('-'*10)
    print(' > Test read h5')
    d2 = Design('IOtest', PRJ_NAME, 'v1b')
    d2.loadDesign('test.h5') # pull d data into d2
    d2.params['bodyLen'].val = 42 # update
    d2.params['phiCruise'].val = 0.7
    #print('x',type(d2.started), d2.started)
    #print('x',type(d2.lastmod), d2.lastmod)

    d2.storeDesign('test2.h5', overwrite=True)  # test read, then save
     
    print('-'*10)
    print(' > test multiple designs/file')
    d3 = Design('New3', PRJ_NAME, 'v2')
    d3.addDesParam('geom', 'tailChord', 3, 'ft')
    d2.cpParams(d3)
    d2.cpConstraints(d3)
    d2.cpMetrics(d3)
    d3.storeDesign('test.h5', overwrite=False) # test append

    print('-'*10) # test error handling
    #d.addDesParam('test', 'dmy', 100, 'g') # raises error for units
