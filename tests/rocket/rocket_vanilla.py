import pyCAPS

import os
import sys
import shutil


cwd = os.path.dirname(__file__)
PRJ = 'DEMO'
rocket_csm = 'rocket.csm'
csm_pth = os.path.join(cwd, rocket_csm)


print(' > init CAPS problem')
prob = pyCAPS.Problem(problemName=PRJ, 
                    capsFile=csm_pth,
                        outLevel=2)



print(' ... cfg egads')
prob.analysis.create(aim = 'egadsTessAIM',
                    name = 'egads',
                    capsIntent = 'CFD')

prob.analysis['egads'].input.Mesh_Format = 'Tecplot'
prob.analysis['egads'].input.Tess_Params = [.6, .05, 20.0]

print(' ... cfg tetgen')
prob.analysis.create(aim =  'tetgenAIM',
                    name =  'tetgen',
                    capsIntent = 'CFD')


prob.analysis['tetgen'].input.Preserve_Surf_Mesh = True
print(' ... link')
prob.analysis['tetgen'].input['Surface_Mesh'].link(prob.analysis['egads'].output['Surface_Mesh'])


# TODO: these run automatically?
# surf mesh
#prob.analysis['egads'].preAnalysis()
#prob.analysis['egads'].postAnalysis()

# vol mesh
#prob.analysis['tetgen'].preAnalysis()
#prob.analysis['tetgen'].postAnalysis()

prob.geometry.save('surf_msh.egads')
#prob.geometry.save('vol_msh.tetgen')