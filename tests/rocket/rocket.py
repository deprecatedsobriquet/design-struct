#!/usr/bin/env python

#import pyCAPS

""" test esp integration,

want to pass from Design() instance to caps to drive ESP changes and generate output"""

import os
from tool.caps import CAPS
from tool.design import Design
from tool.analysis import Analysis
from tool.esp import ESP
#from tool.caps import *
#from tool.esp import *


# init problem
cwd = os.path.dirname(__file__)
PRJ = 'DEMO'
rocket_csm = 'rocket.csm'

design = Design('rocket', PRJ, 'v0')
design.addDesParam('geom', 'fin:nr', 4, 'NA' )
design.addDesParam('geom', 'fin:b', 12, 'in')
design.addDesParam('geom', 'cone:len', 48, 'in')
design.addDesParam('geom', 'cone:noserad', 1, 'in')
design.addDesParam('geom', 'bdy:len', 48, 'in')
design.addDesParam('geom', 'bdy:rad', 10, 'in')
design.lstParams()

design.addDesCons('minNrFin',   'fin:nr',       'gt', 0, 'NA')
design.addDesCons('minConeLen', 'cone:len',     'ge', 1, 'in')
design.addDesCons('minBdyLen',  'bdy:len',      'ge', 12, 'in')
design.addDesCons('minBdyRad',  'bdy:rad',      'ge', 1, 'in')
design.addDesCons('minNoseRad', 'cone:noserad', 'ge', 0.125, 'in')
design.addDesCons('maxNoseRad', 'cone:noserad', 'le', 12, 'in')
design.lstDesCons()

design.checkClosure()

design.storeDesign('rocket.h5')


#myProblem = pyCAPS.Problem(problemName=PRJ,
#                           capsFile=rocket_csm)
capProb = None

capProb = CAPS()
capProb.init(PRJ, rocket_csm)
an = Analysis(PRJ, capProb, cwd)
esp = ESP(capProb, rocket_csm) # TODO: pass stuff

an.addTask('geom', esp)

G = an.tasks['geom']

# for pname in design.params
#    if pname in ESP
#       ESP.setPmtr(design, pname) # TODO : ??? 
# ##      myProblem.geometry.despmtr[pname] = design.params[pname].val

G.update(design) # pass parameters to ESP

an.run()


#print ("Saving geometry")
#myProblem.geometry.save("rocket", directory = cwd, extension = "step")

G.save(cwd, "rocket", "step")


# get xcg, ycg, zcg, Ixx, ...

